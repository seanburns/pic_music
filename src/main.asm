;
;                          PIC12F629
;                       _______  _______
;                      |       \/       |
;              +5.5V --+ 1            8 +-- 0V
;                      |                |
;       Piezo <> GP5 --+ 2            7 +-- GP0 <> Piezo
;                      |                |
;       (Unused) GP4 --+ 3            6 +-- GP1 ~~ ICSPCLK
;                      |                |
;     ICSPVpp ~~ GP3 --+ 4            5 +-- GP2 (Unused)
;                      |                |
;                      +----------------+
;
;           Circuit Description:
;           ====================
;           Pin 1 supplied by 6V batteries via a 1N4148 diode.
;           Pin 2 directly connected to one end of a piezo transducer.
;           Pin 3 unconnected.
;           Pin 4 is used only for ICSP and so goes directly to an ICSP header pin.
;           Pin 5 unconnected.
;           Pin 6 is used only for ICSP and so goes directly to an ICSP header pin.
;           Pin 7 directly connected to the other end of a piezo transducer.
;
;
;           Pic Resources:
;           ==============
;           Timer 1 is unused.
;           Timer 0 is used for general timing.
;           INT is unused.
;

#include <../inc/mpasmx.inc>   ;Processor specific variable definitions.

    errorlevel  -302         ;Suppress message 302 (ensure that bank bits are correct) from list file.
    errorlevel  -205         ;Suppress warning 205 (found directive in column 1) from list file.

    IFDEF __12F629
        #define __DEVICE_CHOICE_OK
        __CONFIG    _cpd_off & _cp_off & _boden_off & _mclre_off & _pwrte_off & _wdt_off & _intrc_osc_noclkout
    ENDIF
            
            

;********************************************************************************************************************************
;* Macro Definitions                                                                                                            *
;********************************************************************************************************************************
mute        macro                   ;
            bsf     muted           ;
            movlw   b'011110'       ;
            andwf   gpiocopy,f      ;
            movf    gpiocopy,w      ;
            movwf   gpio            ;
            endm                    ;

unmute      macro                   ;
            bcf     muted           ;
            movlw   b'100000'       ;
            iorwf   gpiocopy,f      ;
            movlw   b'111110'       ;
            andwf   gpiocopy,f      ;
            movf    gpiocopy,w      ;
            movwf   gpio            ;
            endm                    ;

play_note   macro   t0reload_param,t0_icreload_param,duration
            movlw   t0reload_param      ;
            movwf   t0reload            ;
            movlw   t0_icreload_param   ;
            movwf   t0icreload          ;
            movlw   duration            ;
            call    PlayTone            ;
            movlw   .1                  ;10ms silence.
            call    Pause               ;
            endm

play_pause  macro   length
            movlw   length          ;
            call    Pause           ;
            endm
            
            
;*******************************************************************************************************************************
;* End of Macro Definitions                                                                                              *
;*******************************************************************************************************************************

            cblock 0x20             ;Store variables above control registers
                    gpiocopy        ;To change the state of a GPIO output pin, modify gpiocopy then copy to gpio register.
                    t0intcount      ;Decrementing count of number of timer 0 interrupts required before 10ms period is complete.
                    t0reload        ;Timer 0 is reloaded with this value after overflow/interrupt. Change this to change audio freq.
                    t0icreload      ;t0intcount is reloaded with this value after it has been decremented to zero.
                    ms10pcount      ;Decrementing count of 10ms periods before 1s period complete.
                    seconds         ;Decrementing count of seconds before alarm timeout.
                    flags           ;General flags.
                    uSecCount       ;
                    mSecCount       ;
                    quartCount      ;
                    Ten_mS_Count    ;
            endc

            #define muted       flags,0     ;

            
            ;Timer 0 reload values to generate frequencies of APPROXIMATE musical notes.
            ;       Note    T0 Reload   Number of t0 ints which = 10 mS
            ;               (t0reload)  (t0icreload)


            #define C1      .17,        .42
            #define Cs1     .30,        .44
            #define D1      .43,        .47
            #define Ds1     .55,        .50
            #define E1      .66,        .53
            #define F1      .77,        .56
            #define Fs1     .87,        .59
            #define G1      .97,        .63
            #define Gs1     .106,       .67
            #define A1      .114,       .70
            #define As1     .122,       .75
            #define B1      .129,       .79
            #define C2      .137,       .84
            #define Cs2     .143,       .88
            #define D2      .150,       .94
            #define Ds2     .156,       .100
            #define E2      .161,       .105
            #define F2      .167,       .112
            #define Fs2     .171,       .118
            #define G2      .176,       .125
            #define Gs2     .181,       .133
            #define A2      .185,       .141
            
            ;#define t0_reload   .156        ;These reload values determine the note or frequency of buzzing.
            ;#define t0_icreload .100        ;

            org     0x00            ;Reset vector.
            goto    start           ;Goto start of code.

            org     0x04            ;Interrupt vector


;********************************************************************************************************************************
;* Initialisation                                                                                                               *
;********************************************************************************************************************************
start       bsf     status,rp0      ;Select Register Bank 1.
            ;call    0x3ff           ;Fetch Oscillator Calibration value.
            movlw   .0
            movwf   osccal          ;And write into calibration register.
            bcf     status,rp0             ;Select Register Bank 0.

            movlw   b'000000'       ;Reset all GPIOcopy bits to 0.
            movwf   gpiocopy        ;
            movwf   gpio            ;Copy the GPIOcopy register to the GPIO register.

            movlw   0x7             ;
            movwf   cmcon           ;Comparator module off.

            bsf     status,rp0             ;Select Register Bank 1.

            movlw   b'011110'       ;Set all GPIO pins to inputs to start with except for piezo pins.
            movwf   trisio          ;

            movlw   b'000000'       ;Disable all GPIO pull-ups.
            movwf   wpu             ;

            movlw   b'11011010'     ;Pullups disabled, INT = Rising edge, Tmr0 prescale = 1, Tmr0 uses ins cycle.
            movwf   option_reg      ;

            bcf     status,rp0             ;

            movlw   b'01000000'     ;Configure timer 1...
            movwf   t1con           ;Timer 1 is gated by T1G pin, No prescale, Internal ins cycle clock.

            movlw   b'00000000'     ;Disable Global interrupts, Disable INT pin interrupts.
            movwf   intcon          ;

            ;movlw   t0_reload       ;These two reload values are related and will be...
            ;movwf   t0reload        ;...retrieved from the eeprom in the final project.
            ;movlw   t0_icreload     ;
            ;movwf   t0icreload      ;

;********************************************************************************************************************************
;* End Of Initialisation                                                                                                        *
;********************************************************************************************************************************

;********************************************************************************************************************************
;* OnState                                                                                                                      *
;********************************************************************************************************************************
onstate     ;* Go to sleep but if water is detected then goto alarm;

#ifdef DEMO_SEQUENCE
            play_note   C1,     .50
            play_note   Cs1,    .50
            play_note   D1,     .50
            play_note   Ds1,    .50
            play_note   E1,     .50
            play_note   F1,     .50
            play_note   Fs1,    .50
            play_note   G1,     .50
            play_note   Gs1,    .50
            play_note   A1,     .50
            play_note   As1,    .50
            play_note   B1,     .50
            play_note   C2,     .50
            play_note   Cs2,    .50
            play_note   D2,     .50
            play_note   Ds2,    .50
            play_note   E2,     .50
            play_note   F2,     .50
            play_note   Fs2,    .50
            play_note   G2,     .50
            play_note   Gs2,    .50
            play_note   A2,     .50
#endif
            
bar1        play_pause          .175
            play_note   G1,     .25

bar2        play_note   E2,     .25
            play_note   E2,     .25
            play_note   D2,     .25
            play_note   C2,     .25
            play_note   D2,     .25
            play_note   A1,     .75

bar3        play_pause          .25
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   E2,     .25
            play_note   D2,     .50
            play_note   D2,     .50

bar4        play_note   E2,     .100
            play_pause          .100

bar5        play_pause          .175    ;Same as bar1.
            play_note   G1,     .25

bar6        play_note   E2,     .25     ;Same as bar2.
            play_note   E2,     .25
            play_note   D2,     .25
            play_note   C2,     .25
            play_note   D2,     .25
            play_note   A1,     .75

bar7        play_pause          .25     ;Same as bar3.
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   E2,     .25
            play_note   D2,     .50
            play_note   D2,     .50
            
bar8        play_note   E2,     .100    ;Same as bar4.
            play_pause          .100

bar9        play_pause          .175    ;Same as bar5.
            play_note   G1,     .25
            
bar10       play_note   E2,     .25     ;New bar.
            play_note   E2,     .25
            play_note   D2,     .25
            play_note   C2,     .25
            play_note   D2,     .100

bar11       play_pause          .25     ;Same as bar3.
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   E2,     .25
            play_note   D2,     .50
            play_note   D2,     .50

bar12       play_note   E2,     .100    ;Same as bar4.
            play_pause          .100

bar13       play_pause          .175    ;Same as bar1.
            play_note   G1,     .25

bar14       play_note   E2,     .25     ;Same as bar2.
            play_note   E2,     .25
            play_note   D2,     .25
            play_note   C2,     .25
            play_note   D2,     .25
            play_note   A1,     .75

bar15       play_pause          .25     ;Same as bar3.
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   E2,     .25
            play_note   D2,     .50
            play_note   D2,     .50
            
bar16       play_note   E2,     .100    ;Same as bar4.
            play_pause          .100
            
bar17       play_pause          .200    ;New bar.
            
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   F2,     .50
            
            play_note   F2,     .25
            play_note   E2,     .25
            play_note   D2,     .25
            play_note   E2,     .25
            play_note   D2,     .50
            
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   F2,     .25
            play_note   F2,     .50
            
            play_note   F2,     .25
            play_note   G2,     .25
            play_note   D2,     .25
            play_note   C2,     .25
            play_note   D2,     .25
            play_note   C2,     .50
            
            
Silence1    movlw   .25             ;250ms silence.
            call    Pause           ;
            
me          goto    me




;********************************************************************************************************************************
;* ALARMSTATE                                                                                                                   *
;********************************************************************************************************************************
alarmstate  ;* Sound Alarm at configurable frequencies.

            movf    t0icreload,w    ;
            movwf   t0intcount      ;

            movlw   .100            ;Reload ms10pcount (10ms period counter) with 100 (100 x 10ms = 1 Sec).
            movwf   ms10pcount      ;

            unmute                  ;Unmute the piezo.

alarmloop

waitT0      btfss   intcon,t0if            ;Wait here until overflow of timer 0, which happens at 2 x current audio frequency.
            goto    waitT0          ;

            movlw   .2              ;Two cycle fix when writing to timer 0 whilst it is running
            addwf   t0reload,w      ;Added to timer 0 reload value.
            addwf   tmr0,f          ;Then added to current timer 0 value.
            bcf     intcon,t0if            ;Clear any outstanding timer 0 overflow flag.

            btfsc   muted           ;
            goto    skiptoggle      ;
            movlw   b'100001'       ;Toggle both of the piezo driving pins in the GPIOcopy register.
            xorwf   gpiocopy,f      ;
            movf    gpiocopy,w      ;Copy GPIOcopy register to GPIO register to update the output pins.
            movwf   gpio            ;

skiptoggle  decfsz  t0intcount,f    ;Decrement timer 0 interrupt counter and if it hasn't reached 0 then repeat alarmloop.
            goto    alarmloop       ;

            movf    t0icreload,w    ;Reload t0intcount with t0icreload.
            movwf   t0intcount      ;

every10ms   clrwdt                  ;

            decfsz  ms10pcount,f    ;Decrement counter of 10ms periods and if it hasn't reached 0 then repeat alarmloop.
            goto    alarmloop       ;

            movlw   .100            ;Reload ms10pcount (10ms period counter) with 100 (100 x 10ms = 1 Sec).
            movwf   ms10pcount      ;

;new temporary code for test purposes:-
;maybe25    movlw   .25             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe24         ;
;           movlw   .97
;           movwf   t0reload
;           movlw   .63
;           movwf   t0icreload
;
;maybe24    movlw   .24             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe23         ;
;           movlw   .143
;           movwf   t0reload
;           movlw   .88
;           movwf   t0icreload
;
;maybe23    movlw   .23             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe22         ;
;           movlw   .150
;           movwf   t0reload
;           movlw   .94
;           movwf   t0icreload
;
;maybe22    movlw   .22             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe21         ;
;           movlw   .156
;           movwf   t0reload
;           movlw   .100
;           movwf   t0icreload
;
;
;maybe21    ;8*     G               97          63
;           ;14*    C#              143         88
;           ;15*    D               150         94
;           ;16*    D#              156         100
;
;
;           movlw   .21             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe20         ;
;           movlw   .97
;           movwf   t0reload
;           movlw   .63
;           movwf   t0icreload
;
;maybe20    movlw   .20             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe19         ;
;           movlw   .143
;           movwf   t0reload
;           movlw   .88
;           movwf   t0icreload
;
;maybe19    movlw   .19             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe18         ;
;           movlw   .150
;           movwf   t0reload
;           movlw   .94
;           movwf   t0icreload
;
;maybe18    movlw   .18             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe17         ;
;           movlw   .156
;           movwf   t0reload
;           movlw   .100
;           movwf   t0icreload
;
;maybe17    ;8*     G               97          63
;           ;14*    C#              143         88
;           ;15*    D               150         94
;           ;16*    D#              156         100
;           movlw   .17             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe16         ;
;           movlw   .97
;           movwf   t0reload
;           movlw   .63
;           movwf   t0icreload
;
;maybe16    movlw   .16             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe15         ;
;           movlw   .143
;           movwf   t0reload
;           movlw   .88
;           movwf   t0icreload
;
;maybe15    movlw   .15             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe14         ;
;           movlw   .150
;           movwf   t0reload
;           movlw   .94
;           movwf   t0icreload
;
;maybe14    movlw   .14             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe13         ;
;           movlw   .156
;           movwf   t0reload
;           movlw   .100
;           movwf   t0icreload
;
;maybe13    goto    skippy
;           movlw   .13             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe12         ;
;           movlw   .137
;           movwf   t0reload
;           movlw   .84
;           movwf   t0icreload
;
;maybe12    movlw   .12             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe11         ;
;           movlw   .143
;           movwf   t0reload
;           movlw   .88
;           movwf   t0icreload
;
;maybe11    movlw   .11             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe10         ;
;           movlw   .150
;           movwf   t0reload
;           movlw   .94
;           movwf   t0icreload
;
;maybe10    movlw   .10             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe09         ;
;           movlw   .156
;           movwf   t0reload
;           movlw   .100
;           movwf   t0icreload
;
;maybe09    movlw   .09             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe08         ;
;           movlw   .161
;           movwf   t0reload
;           movlw   .105
;           movwf   t0icreload
;
;maybe08    movlw   .08             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe07         ;
;           movlw   .167
;           movwf   t0reload
;           movlw   .112
;           movwf   t0icreload
;
;maybe07    movlw   .07             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe06         ;
;           movlw   .171
;           movwf   t0reload
;           movlw   .118
;           movwf   t0icreload
;
;maybe06    movlw   .06             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe05         ;
;           movlw   .176
;           movwf   t0reload
;           movlw   .125
;           movwf   t0icreload
;
;maybe05    movlw   .05             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe04         ;
;           movlw   .181
;           movwf   t0reload
;           movlw   .133
;           movwf   t0icreload
;
;maybe04    movlw   .04             ;
;           xorwf   seconds,w       ;
;           btfss   z               ;
;           goto    maybe03         ;
;           movlw   .185
;           movwf   t0reload
;           movlw   .141
;           movwf   t0icreload
;maybe03


;end of temporary code for test purposes.


            decfsz  seconds,f       ;
            goto    alarmloop       ;
            goto    alarmloop        ;
;********************************************************************************************************************************
;* End Of AlarmState                                                                                                            *
;********************************************************************************************************************************



;********************************************************************************************************************************
;* MuteState                                                                                                                    *
;********************************************************************************************************************************
mutestate   ;After changing to 'mutestate' wait for rain to NOT be detected for 2.5 secs and then play tune and goto off mode.
            clrwdt                  ;Clear WDT.
            mute                    ;
            movlw   .199            ;Initialise 5 uSec counter.
            movwf   uSecCount       ;
            movlw   .250            ;Initialise mSec counter.
            movwf   mSecCount       ;
            movlw   .10             ;
            movwf   quartCount      ;Initialise quart(er second) counter. (10 = 2.5 secs)

muteloop    btfsc   gpio,gp2             ;Skip if no water on probe.
            goto    mutestate       ;Otherwise go to 'mutestate' so that timer can be restarted from beginning.

            decfsz  uSecCount,f     ;
            goto    muteloop        ;5us x 199 - 1us = 994us.
            clrwdt                  ;

            movlw   .199            ;2 us.
            movwf   uSecCount       ;

            decfsz  mSecCount,f     ;
            goto    muteloop        ;

            movlw   .250            ;
            movwf   mSecCount       ;

            decfsz  quartCount,f    ;
            goto    muteloop        ;

            ;Future update: Play the 'stopped raining tune' before going to 'onstate'.
;********************************************************************************************************************************
;* End Of MuteState                                                                                                             *
;********************************************************************************************************************************


            goto    onstate





;;;;;;;;;;;;;;;;;;;;  PlayTone  ;;;;;;;;;;;;;;;;;
PlayTone    ;t0reload,t0icreload = tone parameters. W = multiple of 10ms of required tone duration.
            movwf   mS10pcount      ;
            movf    t0icreload,w    ;
            movwf   t0intcount      ;
            unmute                  ;

BeepLoop

BwaitT0     btfss   intcon,t0if            ;Wait here until overflow of timer 0, which happens at 2 x current audio frequency.
            goto    BwaitT0         ;
    
            movlw   .2              ;Two cycle fix when writing to timer 0 whilst it is running
            addwf   t0reload,w      ;Added to timer 0 reload value.
            addwf   tmr0,f          ;Then added to current timer 0 value.
            bcf     intcon,t0if            ;Clear any outstanding timer 0 overflow flag.
    
    
            movlw   b'100001'       ;Toggle both of the piezo driving pins in the GPIOcopy register.
            xorwf   gpiocopy,f      ;
            movf    gpiocopy,w      ;Copy GPIOcopy register to GPIO register to update the output pins.
            movwf   gpio            ;
    
            decfsz  t0intcount,f    ;Decrement timer 0 interrupt counter and if it hasn't reached 0 then repeat BeepLoop.
            goto    BeepLoop        ;
    
            movf    t0icreload,w    ;Reload t0intcount with t0icreload.
            movwf   t0intcount      ;
    
Bevery10mS  clrwdt                  ;
            decfsz  mS10pcount,f    ;Decrement counter of 10mSec periods and if it hasn't reached 0 then repeat alarmloop.
            goto    BeepLoop        ;
    
            mute                    ;
            return                  ;

;;;;;;;;;; End PlayTone  ;;;;;;;;;;;;;;;;;;;;




Pause       movwf   Ten_mS_Count    ;2us for Call + 1us for this line.

PauseLoop   movlw   .10             ;PauseLoop = (9996 * Ten_mS_Count) - 1us.
            movwf   mSecCount       ;

_10mLoop    movlw   .199            ;_10mLoop = (999us x 10) - 1us = 9989us.
            movwf   uSecCount       ;0.2us

_1mLoop     clrwdt                  ;_1mLoop = (5 x 199 x 1uS) - 1us = 994 uSecs.
            nop                     ;0.2us
            decfsz  uSecCount,f     ;
            goto    _1mLoop         ;

            decfsz  mSecCount,f     ;
            goto    _10mLoop



            decfsz  Ten_mS_Count,f  ;9989us + 5us = 9994us
            goto    PauseLoop       ;

            return                  ;2us.





            org     0x03ff
            ;retlw   .0




            end



            ;Timer 0 Overflow Interrupt:

;           Toggle piezo driving pins.
;           Increment interrupt counter.
;           If overflow of interrupt counter then:
;                       Reload interrupt counter (using selected reload value)
;                       Increment 10mS counter.
;                       If overflow of 10mS counter then
;                                   Reload it (count 100) and increment Second counter.
;                                   If Second counter has counted to 240 (4 mins) then goto MUTE state.






            ;***** Generate Alarm *****
            ;Set GP5 & GP0 to outputs of opposite polarity.
            ;Every period, determined by timer 0 reload byte, toggle GP5 & GP0.


;           Interrupt every 1/(AudioFreq*2) =

