#Select which PIC device to build for:
PART_NUMBER := 12f629
EXECUTABLE := main

LINKER_MACROS := -u_DEBUG
LINKER_SYMBOLS := -z__ICD2RAM=1 -z__MPLAB_BUILD=1 -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_SIMULATOR=1

SRC_PATH := src
INC_PATH := inc
BIN_PATH := bin

ALL_INC_FILES := $(wildcard $(INC_PATH)/*)

ifdef OS
	# Windows
	TYPE_OR_CAT=type
	RM = del /q
	RMDIR = rmdir /s /q 
	CP = copy
	LINUX_ONLY =
	FixPath = $(subst /,\,$1)
	ASM	= ..\assembler\mpasmx.exe
	LINK = ..\assembler\mplink.exe
	HEX = ..\assembler\mp2hex.exe
	MKDIR =	if not exist $(BIN_PATH) mkdir
else
	ifeq ($(shell uname), Linux)
		# Linux
		TYPE_OR_CAT=cat
		RM = rm -f
		RMDIR = rm -f -r 
		CP = cp
		LINUX_ONLY = if [ $$(stat -L -c %s ../$(BIN_PATH)/$(EXECUTABLE).err) != 0 ]; then ($(TYPE_OR_CAT) $(call FixPath,../$(BIN_PATH)/$(EXECUTABLE).err) && exit 1); else exit 0; fi
		FixPath = $1
		ASM := ../assembler/mpasmx
		LINK = ../assembler/mplink
		HEX = ..\assembler\mp2hex
		MKDIR =	mkdir -p 
	endif
endif


# Enable list file to be output.
override ASMFLAGS += -l"../$(BIN_PATH)/$(EXECUTABLE).lst"

# Enable error file to be output.
override ASMFLAGS += -e"../$(BIN_PATH)/$(EXECUTABLE).err"

# Enable object file to be output, i.e. make relocatable code.
override ASMFLAGS += -o"../$(BIN_PATH)/$(EXECUTABLE).o"

# Report all messages, warnings & errors.
override ASMFLAGS += -w0

# Do not show a progress window.
override ASMFLAGS += -s-

# Disable case sensitivity.
override ASMFLAGS += -c-

# Assemble the assembly source file into a programmable Intel-based hex file.
$(BIN_PATH)/$(EXECUTABLE).hex: $(BIN_PATH)/$(EXECUTABLE).o
	make -C $(INC_PATH) -f  ../makefile link

# Assemble the assembly source file into a programmable Intel-based hex file.
$(BIN_PATH)/$(EXECUTABLE).o: $(SRC_PATH)/$(EXECUTABLE).asm $(ALL_INC_FILES)
	$(MKDIR) $(BIN_PATH)
	make -C $(INC_PATH) -f  ../makefile assemble

assemble:
	$(ASM) $(ASMFLAGS) -p$(PART_NUMBER) ../$(SRC_PATH)/$(EXECUTABLE).asm || ($(TYPE_OR_CAT) $(call FixPath,../$(BIN_PATH)/$(EXECUTABLE).err) && exit 1)
    # For some reason the Linux version of the assembler doesn't return the correct exit codes to indicate failure, so this catches those cases:
	$(LINUX_ONLY)

link:
	$(LINK) -v -p$(PART_NUMBER) $(LINKER_MACROS) $(LINKER_SYMBOLS) -m../$(BIN_PATH)/$(EXECUTABLE).map -o../$(BIN_PATH)/$(EXECUTABLE).cof ../$(BIN_PATH)/$(EXECUTABLE).o -n0

.PHONY : clean
clean :
	$(RMDIR) $(BIN_PATH)
